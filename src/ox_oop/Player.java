/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ox_oop;

/**
 *
 * @author Windows
 */
public class Player {

    private char name;
    private int win, lost, draw;

    public void setName(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void addWin() {
        win++;
    }

    public void addLost() {
        lost++;
    }

    public void addDraw() {
        draw++;
    }

    public int getWin() {
        return win;
    }

    public int getLost() {
        return lost;
    }

    public int getDraw() {
        return draw;
    }
}
